# Solving SAT and MaxSAT with a Quantum Annealer: Foundations and a Preliminary Report 
## Zhengbing Bian, Fabian Chudak, William Macready, Aidan Roy, Roberto Sebastiani and Stefano Varotti

This repository contains supplementary material for the paper, such as software and experimental data.


## Repository index

Source code:

- [Sgen generator](https://bitbucket.org/aqcsat/pysgen)
- [Ubcsat demo script for SAT problems](https://bitbucket.org/aqcsat/frocos2017/src/master/run_saps.sh?fileviewer=file-view-default)
- [Ubcsat demo script for MAXSAT problems](https://bitbucket.org/aqcsat/frocos2017/src/master/run_weighted_maxsat.sh?fileviewer=file-view-default)
- [2-in-4-SAT -> Ising model conversion utility](https://bitbucket.org/aqcsat/frocos2017/src/master/output2in4Ising.m?fileviewer=file-view-default)
- [D-Wave demo script](https://bitbucket.org/aqcsat/frocos2017/src/master/run2in4BenchmarksDemo.m?fileviewer=file-view-default) (requires access to a D-Wave system)

Experimental data:

- [Experiment instances (SAT problems)](https://bitbucket.org/aqcsat/pysgen/src/master/allbenchmarks/)
- [Experiment instances (MAXSAT problems)](https://bitbucket.org/aqcsat/pysgen/src/master/allbenchmarks_maxsat/)
- [Experiment encodings (SAT problems)](https://bitbucket.org/aqcsat/pysgen/src/master/allbenchmarks_ising/) 
- [Experiment encodings (MAXSAT problems)](https://bitbucket.org/aqcsat/pysgen/src/master/allbenchmarks_maxsat_ising/) 

SMT models:

- [Sample .smt files for encoding Boolean functions as Ising models](https://bitbucket.org/aqcsat/frocos2017/src/master/smt/) 

Additional information:

- [Contact information for accessing a D-Wave system](https://bitbucket.org/aqcsat/frocos2017/src/master/DWave_contact.txt?fileviewer=file-view-default)
- [Extended version of FROCOS 2017 paper](https://bitbucket.org/aqcsat/frocos2017/raw/HEAD/sat2ising_extended.pdf)


## sgen generator

The script used for generating experiments is available at https://bitbucket.org/aqcsat/pysgen.
Consult the readme for further information.


## About the experiments

The experiments used in the paper are available inside the pysgen repository, at https://bitbucket.org/aqcsat/pysgen/src/master/benchmarks . A bigger assortment of problems  is available at https://bitbucket.org/aqcsat/pysgen/src/master/allbenchmarks/?at=master (warning: the page is slow to load).
Problems are available in DIMACS and bench formats. 
The filename convention is the following:

```
sgen-TYPE-SIZE-GROUPSIZE-SEED.FMT

```

where:

* TYPE: type of constraints used, can be "base" or "twoinfour"

* SIZE: size of the problem (# of variables)

* GROUPSIZE: size of the partitioning groups (4,5 or 6 for base instances, 4 for twoinfour)

* SEED: random seed

* FMT: file format, cnf/wcnf for DIMACS files or bench for bench files

To benchmark ubcsat on these experiment you can consult the script at https://bitbucket.org/aqcsat/frocos2017/src/master/run_saps.sh?fileviewer=file-view-default .



## Encodings (Ising models and embeddings)

The conversion utility https://bitbucket.org/aqcsat/frocos2017/src/master/output2in4Ising.m?fileviewer=file-view-default takes a 2-in-4-SAT instance in .bench format and constructs an Ising model whose ground states correspond to satisfiability. The Ising models constructed for the SAT experiments in the paper are here: https://bitbucket.org/aqcsat/pysgen/src/master/allbenchmarks_ising/. 

In order to solve SAT problems using D-Wave hardware, the Ising models must be embedded. The embeddings used in the experiments are also in https://bitbucket.org/aqcsat/pysgen/src/master/allbenchmarks_ising/. A demonstration script (https://bitbucket.org/aqcsat/frocos2017/src/master/run2in4BenchmarksDemo.m?fileviewer=file-view-default) shows how the D-Wave hardware is used; actually running the script requires the D-WAVE solver programming interface (SAPI) and access to a D-Wave machine.