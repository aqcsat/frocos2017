## SMT files for "Solving SAT and MaxSAT with a Quantum Annealer: Foundations and a Preliminary Report" 

This directory contains some SMT files which represent the problem of encoding a Boolean function as Ising model.

- [2in4sat_gap2.smt2](https://bitbucket.org/aqcsat/frocos2017/src/master/smt/2in4sat_gap2.smt2?fileviewer=file-view-default): Encoding a 2-in-4-SAT constraint in an Ising model with the structure of a K(3,3) complete bipartite graph. This penalty model was used to conduct the SAT experiments in the paper.

- [2bits_times_2bits_gap1.5.smt2](https://bitbucket.org/aqcsat/frocos2017/src/master/smt/2bits_times_2bits_gap1.5.smt2?fileviewer=file-view-default): encodes a (2-bit)x(2-bit) multiplication circuit in a K(8,8) complete bipartite graph. 

- [2bits_times_2bits_gap1.5_varorder2.smt2](https://bitbucket.org/aqcsat/frocos2017/src/master/smt/2bits_times_2bits_gap1.5_varorder2.smt2?fileviewer=file-view-default): encodes a (2-bit)x(2-bit) multiplication circuit in a K(8,8) complete bipartite graph, but with a different variable elimination order. This problem takes ~8 minutes to solve on a laptop using MathSAT, while the previous variable order takes much longer.
