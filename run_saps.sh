#!/bin/sh
# run this script in the pysgen main folder to run the experiments

mkdir -p reports
for i in benchmarks/*.cnf;
do 
    BASEI=$(basename $i);
    ubcsat -alg saps -cutoff max -runs 1000 -r rtd  -i $i > reports/saps_${BASEI%.cnf}.txt;
done
